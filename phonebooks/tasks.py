from django.conf import settings
from phonebooks.models import MainModel, CountryModel
from django.core.cache import cache
from celery.task import PeriodicTask
from datetime import timedelta
from celery.utils.log import get_task_logger
import sqlite3

logger = get_task_logger(__name__)

class ProcessMainModelUpdateTask(PeriodicTask):
    run_every = timedelta(days=1)

    def run(self, **kwargs):
        read_main_model()

def read_main_model():

    conn = sqlite3.connect(settings.PHONEBOOKS_MAIN_MODEL_PATH)
    c = conn.cursor()
    results = c.execute('select ZCOUNTRY_CODE, ZUUID from ZPOIMODEL;')
    for row in results:
        if not CountryModel.objects.filter(uuid=row[1]).exists():
            CountryModel.objects.create(country_code=row[0], uuid=row[1])
    result = c.execute('select Z_VERSION, Z_UUID from Z_METADATA;').fetchone()

    mm = MainModel.objects.last()
    if mm is None:
        mm = MainModel.objects.create(version=result[0], uuid=result[1])
    mm.version = result[0]
    mm.uuid = result[1]
    mm.save()
    logger.info('Running database update. Main model version: {}, uuid: {}', mm.version, mm.uuid)
