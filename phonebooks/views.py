from django.http import HttpResponse, JsonResponse
from django.views import View
from phonebook import settings
from phonebooks.models import MainModel, CountryModel



class PhonebookMain(View):

    def get(self, request):
        """
        Phonebook main model getter.
        If 'version' keywork is specified in the query with any value,
        then the current server main model version is returned.
        Otherwise, the main model file itself will be transmitted to the client

        :param request: version
        :return:
        """
        v = request.query_params.get('version', None)
        if v:
            model = MainModel.objects.last()
            if model is None:
                raise IndexError("No main model was found")
            return JsonResponse({
                'version' : model.version
            }, status=200)
        else:
            response = HttpResponse(content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename="Main.Model"'
            response['X-Accel-Redirect'] = "{}Main.Model".format(settings.PHONEBOOKS_URL_PATH)
            return response


class PhonebookCountry(View):

    def get(self, request, uuid):
        """
        Phonebook Country Model getter.
        The specified uuid maps to the local file via database
        Returnes the located file if successful

        :param request:
        :param uuid: uuid
        :return:
        """
        if CountryModel.objects.filter(uuid=uuid).exists():
            model = CountryModel.objects.get(uuid=uuid)
            response = HttpResponse(content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename="{}.Model"'.format(model.country_code)
            response['X-Accel-Redirect'] = "{}{}.Model".format(settings.PHONEBOOKS_URL_PATH, model.country_code)
            return response
        else:
            return JsonResponse({'detail' : 'Model with provided uuid was not found'}, status=404)