from __future__ import unicode_literals

from django.db import models


class MainModel(models.Model):
    version = models.CharField(max_length=128)
    uuid = models.CharField(max_length=255, null=True)
    updated = models.DateTimeField(auto_now=True)


class CountryModel(models.Model):
    country_code = models.IntegerField(default=0)
    uuid = models.CharField(max_length=255, default='', unique=True)